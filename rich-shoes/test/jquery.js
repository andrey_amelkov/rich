$(document).ready(function(){
	$(window).resize(function(){
		var _width = 0,
			_delta;
		if ($(window).width() < 320) {
			_delta = '100%';
		}
		else {
			_delta = (($(window).width() / 320) * 100) + '%';
		}
		$('html').css('font-size',_delta);
	});
	$(window).trigger('resize');
	$('form.search input[type=text]').focus(function(){
		$(this).parents('form.search').addClass('focus')
	}).blur(function(){
		$(this).parents('form.search').removeClass('focus')
	});
});
