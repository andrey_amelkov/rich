<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<?PHP //header("Content-Type: text/html; charset=utf-8");?>

<?php include_once 'admin/config.php' ?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title></title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>

    <?php include_once 'blocks/head.php' ?>
</head>

<body>

<div id="wrapper" class="page_<?php print ($_REQUEST['page']) ?>">

    <div id="header">


        <div class="sub-head">

            <a class="logo"  href="/"><span data-icon="&#x26;" aria-hidden="true"></span></a>

            <div class="hleft">
                <span>+7 9115 456 783</span>
                <a href="mailto:buy@richshoes.ru">BUY@RICHSHOES.RU</a>

                <div class="mobile mobile-size">
                    <div>
                        Ваш размер
                    </div>
                    <select name="size" id="select_size">
                        <option value="0">Все</option>
                        <?php foreach ($size_select as $val) :?>
                            <option value="<?php print $val ?>"><?php print $val ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

            </div>


            <div class="hright">

                <div class="mobile mobile-link">
                    Следите за нами!
                </div>

                <a class="link1" href="http://instagram.com/richshoesru">
                    <span class="ico" data-icon="&#x27;" aria-hidden="true"></span><span class="word">ИНСТАГРАМ</span>
                </a>

                <a class="link2" href="http://vk.com/richshoes">
                    <span class="ico" data-icon="&#x22;" aria-hidden="true"></span><span class="word">ВКОНТАКТЕ</span>
                </a>

            </div>


           <br class="clear">
        </div>


        <div class="block-menu">

            <div class="mobile mobile_menu">
                <a href="#">Меню</a>
            </div>

            <ul class="menu">

                <?php  $rez = return_rez("SELECT id,title FROM pages ");  ?>

                <?php foreach ($rez as $key => $row) : ?>
                    <?php //dpm($key) ?>
                    <li <?php if ($_REQUEST['page'] == ($row['id'])): ?>class="active" <?php endif;?>><a href="index.php?page=<?php print($row['id']); ?>"><?php print($row['title']); ?></a></li>
                    <?php if (($key + 1) < count($rez)) : ?>
                        <li class="sepa"></li>
                    <?php endif; ?>

                <?php endforeach;?>

            </ul>
        </div>
    </div>
    <!-- #header-->

    <div id="content">
        <div class="sub-content">

            <?php include_once 'blocks/content.php' ?>

        </div>

    </div>
    <!-- #content-->

</div>
<!-- #wrapper -->

<div id="footer">
    <div>
        <a href="mailto:buy@richshoes.ru">BUY@RICHSHOES.RU</a>
        <a href="">КАЛИНИНГРАДСКИЙ ПАССАЖ  <span  data-icon="&#x28;" aria-hidden="true"></span> ПАВИЛЬОН №301</a>
        <a href="http://vk.com/richshoes">VK.COM/RICHSHOES</a>
    </div>
</div>
<!-- #footer -->

</body>
</html>