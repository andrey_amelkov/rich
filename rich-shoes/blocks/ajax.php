<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/admin/config.php';

if (isset($_POST)) {
    $data = $_POST;
    $link_product = "<a href = '"."http://" . $_SERVER['HTTP_HOST']."?product=".$data['scu']."'>".$data['scu']."</a>";
//    $link_product = '<a href="asd">ссылка на товар</a>';

    //админу
    $text1 = sprintf("
        Артикул: %s <br>
        Размер: %s <br>
        Email клиента: %s <br>
        Телефон клиента: %s <br>
        Имя клиента: %s <br>
        Комментарий к заказу: %s <br>",
        $link_product,$data['size'],$data['email'],$data['phone'],$data['name'],$data['comment']
    );



    $text2 = sprintf("
        Здравствуйте, %s! <br>
        Ваш заказ в магазине Rich принят. Спасибо за покупку! <br>
        Артикул: %s <br>
        Размер: %s <br>
        Email клиента: %s <br>
        Телефон клиента: %s <br>
        Имя клиента: %s <br>
        Комментарий к заказу: %s <br>
        Мы свяжемся с вами в ближайшее время. <br>
        ",
        $data['name'],$link_product,$data['size'],$data['email'],$data['phone'],$data['name'],$data['comment']
    );


    // это админу
    send_mime_mail('Новый заказ',
        $admin_mail,
        'Rich',
        $admin_mail,
        'UTF-8', // кодировка, в которой находятся передаваемые строки (если будет абракодабра поменять наCP1251)
        'CP1251', // кодировка, в которой будет отправлено письмо
        'Новый заказ  в магазине Rich',
        $text1);


    if (isset($data['email'])) {

    // это клиенту
    send_mime_mail('Ваш заказ',
        $admin_mail,
        $data['name'],
        $data['email'],
        'UTF-8', // кодировка, в которой находятся передаваемые строки (если будет абракодабра поменять наCP1251)
        'CP1251', // кодировка, в которой будет отправлено письмо
        'Ваш заказ в магазине Rich принят!',
        $text2);

    }


}

?>

<div class="mess_happy">
    <div class="mess_ok"></div>
    <div class="mess_title">
        ПОЗДРАВЛЯЕМ!
    </div>
    <div class="mess_body">
        Вы благополучно разместили заказ. <br>
        наш консультант скоро свяжется с вами <br>
        по телефону или e-мейл.
    </div>

</div>