<?php
/*
 * Контент фронтенда
 */
$page = ($_REQUEST['page']>0) ? true : false;

$product = $_REQUEST['product'] ;

if ($page)  $rez = return_rez ("SELECT * FROM pages WHERE id = ".$_REQUEST['page']);

?>

<?php if ($page) :?>

    <h1 class="title">
        <span><?php print $rez[0]['title'] ?></span>
    </h1>

    <div class="sub-content <?php if ($page):?>page<?php endif?>">
        <?php print $rez[0]['body'] ?>
    </div>

<?php elseif($product) : ?>

    <?php

    //check_img($product);  // проверка сущ файла

    ?>

    <div class="full_product">
        <?php include_once 'product_item/product_'.$product.'.php'; ?>
    </div>

<?php else:?>

    <h1 class="title-main">
        Наш онлайн-бутик предлагает только оригинальную обувь премиум класса из Италии
    </h1>

    <div class="sub-content">

        <div class="filter-size center">

        Вам подходит
            <select name="size" class="select_size">
                <option value="0">Все</option>
                <?php foreach ($size_select as $val) :?>
                    <option value="<?php print $val ?>"><?php print $val ?></option>
                <?php endforeach; ?>

            </select>
        размер обуви

        </div>

        <div class="filter-cat hidden">

            <div class="left">
                <a class="cat_m" href="#">Женская <span>коллекция</span></a>
            </div>

            <div class="right">
                <a class="cat_w" href="#">Мужская <span>коллекция</span></a>
            </div>

        </div>

        <div class="products hidden">
            <?php include_once 'catalog.php' ?>
        </div>

    </div>

<?php endif;?>