<div class="hidden product">

    <div class="left">
        <div class="fproduct_img">
            <img src="<?php print $mail_url ?>/products/preset_400x400/PFMU4-400x400.jpg" alt=""/>
        </div>
    </div>

    <div class="right product-detals">
        <div class="fproduct_title center">
            CESARE PACIOTTI
        </div>
        <div class="fproduct_vk center">
            <img src="<?php print $mail_url ?>/img/vk_decor.png" alt=""/>
        </div>

        <div class="fproduct_descr">

            Бренд Alla Pugachova,   известной певицы и обувной корпорации «Эконика». Первым результатом их совместного творчества стала коллекция обуви, увидевшая свет в 1997 году. Алла Борисовна самолично подбирала материалы, рисовала эскизы, отслеживала производственный процесс. Дебют оказался успешным, что вдохновило певицу выпускать каждый сезон новые коллекции обуви, сумочек и аксессуаров. Возрастная группа покупательниц бренда обширна – от молоденьких девушек до солидных дам.

        </div>

        <div class="fproduct_size center">
            <span class="small">Доступные размеры</span> <span class="big">34,36</span>
        </div>

        <div class="fproduct_price center">
            <span class="small">Цена:</span> <span class="price_new">25000</span> <span class="price_old">30000</span>
        </div>

        <div class="form_buy">
            <form action="">
                <div class="hidden">
                    
                    <div class="left">

                        <div class="form_col">
                            <div class="form_item_title">
                                Е-МЕЙЛ <span class="red"></span>
                            </div>
                            <div class="form_item_text">
                                <input type="text" name="email" />
                            </div>
                        </div>

                        <div class="form_col">
                            <div class="form_item_title">
                                ВАШЕ ИМЯ
                            </div>
                            <div class="form_item_text">
                                <input type="text" name="name" />

                            </div>
                        </div>

                    </div>
                    
                    <div class="right">

                        <div class="form_col">
                            <div class="form_item_title">
                                МОБ.ТЕЛЕФОН <span class="red"></span>
                            </div>
                            <div class="form_item_text">
                                <input type="text" name="phone"/>
                            </div>
                        </div>

                        <div class="form_col">
                            <div class="form_item_title">
                                НУЖНЫЕ РАЗМЕРЫ
                            </div>
                            <div class="form_item_text">
                                <input type="text" name="size"/>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="center form_item_title">
                    ВАШИ ПОЖЕЛАНИЯ
                </div>
                <div class="textarea_div">

                    <textarea name="comment"></textarea>
                </div>
                <div class="link-buy">
                    <a href="#">Заказать</a>
                </div>
            </form>
        </div>
        <div class="required_fields center">
            <span class="red"></span> -эти поля необходимы для заполнения
        </div>
    </div>

</div>

<div class="body-slider">

    <div class="flexslider">
        <ul class="slides">
            <li>
                <img src="<?php print $mail_url ?>/products/preset_920x920/PFMU4-920x920.jpg">
            </li>
            <li>
                <img src="<?php print $mail_url ?>/products/preset_920x920/PFR100-920x920.jpg">
            </li>
            <li>
                <img src="<?php print $mail_url ?>/products/preset_920x920/PFR301F-920x920.jpg">
            </li>
        </ul>
    </div>
</div>