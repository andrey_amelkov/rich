jQuery(document).ready(function () {
//    jQuery.noConflict();
//    фильтр М
    jQuery('.cat_m').click(function () {

        var check = jQuery('.select_size :selected').val();

        if (check !== '0') {
            jQuery(".select_size")[0].selectedIndex = 0;
            jQuery(".prod").removeClass('hide');
        }
//        console.log(check);

//        jQuery("#select_size [value='0']").attr("selected", "selected");
        jQuery('.prod_w').removeClass('hide');
        jQuery('.cat_w').removeClass('active');
        jQuery(this).toggleClass('active');
        jQuery('.prod_m').toggleClass('hide');

        return false;

    });


//   фильтр Ж
    jQuery('.cat_w').click(function () {
        var check = jQuery('.select_size :selected').val();
        if (check !== '0') {
                jQuery(".select_size")[0].selectedIndex = 0;
            jQuery(".prod").removeClass('hide');
        }
        jQuery('.prod_m').removeClass('hide');
        jQuery('.cat_m').removeClass('active');
        jQuery(this).toggleClass('active');
        jQuery('.prod_w').toggleClass('hide');
        return false;

    });

    jQuery('.select_size').change(function () {
        jQuery('.cat_w,.cat_m').removeClass('active');
        var filter = jQuery(this).val();

        jQuery('.prod').removeClass('hide'); // очищаю от фильтров

        jQuery('.prod').each(function (index) {

            function in_array(value, array) {
                for (var i = 0; i < array.length; i++) {
                    if (array[i] == value) return true;
                }
                return false;
            }

            var prod = jQuery(this); // продукт
            var find_size = prod.attr('rel'); //его размер
            var mas_size = find_size.split(','); // массив

            if (filter == '0') {
                jQuery('.prod').removeClass('hide')
            } else {
                if (!in_array(filter, mas_size)) {
                    prod.addClass('hide');
                }
            }

        });

    });

    jQuery('.flexslider').flexslider({
        slideshow: false,
        controlNav: true,
        directionNav: true,
        prevText: "Previous",
        nextText: "Next",
        start: function (slider) {
            jQuery('.fproduct_img img').click(function (event) {
                var ind = jQuery(this).index();
                slider.flexAnimate(ind, true);
                jQuery.scrollTo('.body-slider', 500);
            });
        }
//        startAt: 1
    });

    coutnImages = jQuery(".fproduct_img img").length;
    if (coutnImages>1){

        $(".fproduct_img").touchwipe({
            length: coutnImages,
            wipeLeft: function() {
                var vis	= jQuery('.fproduct_img').find('img:visible');
                vis.hide();
                var index = vis.index()+1;
                if (index >= this.length) index = 0;
                jQuery('.fproduct_img img').eq(index).show();

            },
            wipeRight: function() {
                var vis	= jQuery('.fproduct_img').find('img:visible');
                vis.hide();
                var index = vis.index()-1;
                if (index < 0) index = this.length-1;
                jQuery('.fproduct_img img').eq(index).show();
            },
            //  wipeUp: function() { alert("up"); },
            // wipeDown: function() { alert("down"); },
            min_move_x: 20,
            min_move_y: 20,
            preventDefaultEvents: true
        });

        // реакция на миниатюру
        jQuery('.froduct_img_thumb img').click(function (index) {
            jQuery('.fproduct_img').find('img:visible').hide();
            jQuery('.fproduct_img img').eq(jQuery(this).index()).fadeIn();

        });
    }

    jQuery('.fproduct_img img').eq(0).show(); //показываю 1ю



    // обратно к товару
    jQuery('.backto').click(function () {

        var a = jQuery('.flexslider li.flex-active-slide').index();

        jQuery('.fproduct_img img').hide();
        jQuery('.fproduct_img img').eq(a).fadeIn();

        jQuery.scrollTo('#wrapper', 500);
        return false;
    });

    // вопросы и ответы
    jQuery('.page h3').click(function () {
        jQuery(this).next().slideToggle();
        return false;
    });


    function myvalid(){

        function validateEmail(email) //проверка что ящик
        {
            var re = /\S+@\S+\.\S+/;
            return re.test(email);
        }

        var error = "<span class='error'>Неправильно заполненное поле!</span>"

        jQuery('.error').remove(); //удаляю ошибки

        var mail = jQuery('.form_buy input[name="email"]');

        if (validateEmail(mail.val())) {
            mail.removeClass('false');
            mail.next().remove();
            mail.addClass('true');
            var checked1 = true;
        } else {
            mail.addClass('false');
            var checked1 = false;
            mail.after(error).hide().fadeIn(500);
            ;
        }

        var phone = jQuery('.form_buy input[name="phone"]');

        if (parseInt(phone.val()) > 0) {
            phone.next().remove();
            phone.removeClass('false');
            phone.addClass('true');
            var checked2 = true;
        } else {
            phone.addClass('false');
            var checked2 = false;
            phone.after(error).fadeIn(500);
            ;
        }

        var chedked = checked1 * checked2; // общая проверка перед отправкой что ок

        return chedked;

    }


    jQuery('.form_buy input[name="email"],.form_buy input[name="phone"]').focusout(function(){
        myvalid();
    });


//    валидация формы отправки
    jQuery('.link-buy a').click(function () {


        var chedked  = myvalid();

        if (chedked) {

            // данные
            var product = jQuery('.fproduct_title').html();
            var data = jQuery('.form_buy form').serialize();
            // Отсылаем паметры
            jQuery.ajax({
                type: "POST",
                url: "/blocks/ajax.php",
                data: data + '&product=' + product,
                // Выводим то что вернул PHP
                success: function (html) {

                    jQuery(".form_buy").empty();

                    jQuery('.form_buy').append('<div class="decor_load"></div>');
//                        jQuery('.form_buy').delay(1000).empty();

                    setTimeout(function () {
                        jQuery('.form_buy').empty();
                        jQuery(".form_buy").append(html);
                    }, 500);

                }
            });

//            console.log(data);
        }

        return false;
    });


    jQuery('.form_item_title label').click(function () {
        jQuery(this).parent().next().children().focus();
    });


    jQuery('.mobile_menu a').click(function(event){
        var hei = jQuery('.block-menu ul.menu').height();
        event.preventDefault();
        var scrolvalclick = jQuery(window).scrollTop();
        jQuery('.block-menu ul.menu').slideToggle(function(event){
            if(scrolvalclick<100){
                if(hei==270){
                    jQuery('#content').css('margin-top', hei+'px');
                }else{jQuery('#content').css('margin-top', 0+'px');}
            }

        });

    });

    if (jQuery('.mobile_menu').is(':visible')){
    
    jQuery(window).scroll(function(){
        var scrolval = jQuery(window).scrollTop();

//        console.log(scrolval);

        if(scrolval >= 100){
            jQuery('.sub-head').addClass('sub-head2');
            jQuery('.mobile_menu,.mob ul.menu').addClass('fixed_menu');
//            jQuery('.mobile_menu').addClass('fixed_menu');
           jQuery('.block-menu ul.menu').addClass('fixed_menu_drop').css('position', 'fixed');
            jQuery('#content').addClass('content2');
        }
        if(scrolval < 100){
            jQuery('.mobile_menu,.mob ul.menu').removeClass('fixed_menu');
//            jQuery('.mobile_menu,').removeClass('fixed_menu');
            jQuery('.block-menu ul.menu').removeClass('fixed_menu_drop').css('position', 'relative');
            jQuery('.sub-head').removeClass('sub-head2');
            jQuery('#content').addClass('content2')
        }
        });
    }

    jQuery('.page_7 .sub-content > h3').
        wrapInner('<a href="#"></a>').
        append('<span class="contr" data-icon="-" aria-hidden="true"></span>').
        wrapInner('<span class="main-url"></span>')
    ;

    jQuery('.mobile_menu a,.mob li a').append('<span class="point" data-icon="(" aria-hidden="true"></span>').prepend('<span class="point" data-icon="(" aria-hidden="true"></span>');;

//    кнопка назад в мобильной версии при скролле
    jQuery('.return a.return').click(function(){
        jQuery.scrollTo('#wrapper', 500);
        return false;
    });


    //кнопка меню при скролле в мобильной версии
    jQuery('.link_to_menu').click(function(){
        jQuery('ul.menu').slideToggle();
        return false;
    });

    jQuery('.mob-cat').change(function () {

        var filter = jQuery(this).val();
        console.log(filter);
        if (filter=='М') jQuery('.cat_w').click();
        if (filter=='Ж') jQuery('.cat_m').click();
        if (filter=='0') {
            jQuery('.prod_w,.prod_m').removeClass('hide');
            jQuery('.cat_w,.cat_m').removeClass('active');
        };


    });

});
