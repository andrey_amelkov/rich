jQuery(document).ready(function () {

    // jQuery.scrollTo('#wrapper', 0);

    jQuery('.full_product .form_buy div.link-buy').append('<input class="dopsubmit" type="submit" value="ЗАКАЗАТЬ" />');
    jQuery('.full_product .form_buy .link-buy a').remove();
    // jQuery('.dopsubmit').css('display','none');

//    jQuery.noConflict();
//    фильтр М
    jQuery('.cat_m').click(function () {

        var check = jQuery('.select_size :selected').val();

        if (check !== '0') {
            jQuery(".select_size")[0].selectedIndex = 0;
//            jQuery(".prod").removeClass('hide');
        }
//        console.log(check);

        jQuery('.cat_w').removeClass('active');
//        jQuery("#select_size [value='0']").attr("selected", "selected");
        jQuery('.prod_w').removeClass('hide');
        jQuery(this).toggleClass('active');
        jQuery('.prod_m').toggleClass('hide');

        last_item();
        return false;

    });


//   фильтр Ж
    jQuery('.cat_w').click(function () {
        var check = jQuery('.select_size :selected').val();
        if (check !== '0') {
            jQuery(".select_size")[0].selectedIndex = 0;
//            jQuery(".prod").removeClass('hide');
        }
        jQuery('.prod_m').removeClass('hide');
        jQuery('.cat_m').removeClass('active');
        jQuery(this).toggleClass('active');
        jQuery('.prod_w').toggleClass('hide');
        last_item();
        return false;

    });

    jQuery('select.select_size').change(function () {

//        jQuery('.cat_w,.cat_m').removeClass('active');

        var filter = jQuery(this).val();


        jQuery('.prod').removeClass('hide2'); // очищаю от фильтров

        jQuery('.prod').each(function (index) {

            function in_array(value, array) {
                for (var i = 0; i < array.length; i++) {
                    if (array[i] == value) return true;
                }
                return false;
            }

            var prod = jQuery(this); // продукт
            var find_size = prod.attr('rel'); //его размер
            var mas_size = find_size.split(','); // массив

            if (filter == '0') {
                jQuery('.prod').removeClass('hide2')
            } else {
                if (!in_array(filter, mas_size)) {
                    prod.addClass('hide2');
                }
            }

        });
        var val = filter;
        if (val=='0') val = "ВСЕ"
        jQuery('span.select_size').html(val);
        last_item();

    });

    jQuery('.flexslider').flexslider({
        slideshow: false,
        controlNav: true,
        directionNav: true,
        prevText: "Previous",
        nextText: "Next",
        start: function (slider) {

            jQuery(document).keypress(function (e) {
                var a = e.charCode - 49;
                coutnImages = jQuery(".fproduct_img img").length;

                if ((a < coutnImages) 
                    && (a>=0)
                    ) {

                    if (!jQuery('.form_buy input,.form_buy textarea').is(":focus")) {


                        slider.flexAnimate(a, true);

                    }

                }

            });


            jQuery('.fproduct_img img').click(function (event) {
                var ind = jQuery(this).index();
                slider.flexAnimate(ind, true);
                jQuery.scrollTo('.body-slider', 500);
            });

            jQuery('.froduct_img_thumb img').click(function (index) {

                var ind = jQuery(this).index();
                // console.log(ind);
                slider.flexAnimate(ind, true);   
            });

            jQuery('.lupa').click(function () {
                var ind = jQuery('.fproduct_img img:visible').index();
                slider.flexAnimate(ind, true);
                jQuery.scrollTo('.body-slider', 500);

            });
            
            coutnImages = jQuery(".fproduct_img img").length;
        if (coutnImages > 1) {
            $(".fproduct_img").touchwipe({
            length: coutnImages,
            wipeLeft: function () {
                var vis = jQuery('.fproduct_img').find('img:visible');
                vis.hide();
                var index = vis.index() + 1;
                if (index >= this.length) index = 0;
                var sho = jQuery('.fproduct_img img').eq(index);

		
//                alert(index);

                sho.show();
                jQuery('.active_coma').removeClass('active_coma');
                jQuery('a.comas').eq(index).addClass('active_coma');

                slider.flexAnimate(index, true);

            },
            wipeRight: function () {
                var vis = jQuery('.fproduct_img').find('img:visible');
                vis.hide();
                var index = vis.index() - 1;
                if (index < 0) index = this.length - 1;
                jQuery('.fproduct_img img').eq(index).show();
                var sho = jQuery('.fproduct_img img').eq(index);
		
		
//                alert(index);

                sho.show();
                jQuery('.active_coma').removeClass('active_coma');
                jQuery('a.comas').eq(index).addClass('active_coma');
		
                slider.flexAnimate(index, true);

            },
            //  wipeUp: function() { alert("up"); },
            // wipeDown: function() { alert("down"); },
            min_move_x: 20,
            min_move_y: 20,
            preventDefaultEvents: true
        });
        }
        
        },
        after: function(){
            sinh();
        }
    });

    jQuery('.slider_item').flexslider();

    coutnImages = jQuery(".fproduct_img img").length;
    if (coutnImages > 1) {

        

        // реакция на миниатюру
        var wth = jQuery('body').width();
        if (wth < 958) {

            jQuery('.page_product a.return').click(function(){
                location.href="/";
            });

            var html_div = '<div class="comas_disp"><ol>';
            for (var icount = 0; icount < coutnImages; icount++) {
                html_div += '<li><a class="comas">' + icount + '</a></li>';
            }
            html_div += '</ol></div>';
            jQuery('.froduct_img_thumb').append(html_div);
            jQuery('a.comas:first').addClass('active_coma');
            jQuery('.froduct_img_thumb a').click(function (index) {
                jQuery('.active_coma').removeClass('active_coma');
                jQuery(this).addClass('active_coma');
                jQuery('.fproduct_img').find('img:visible').hide();
                jQuery('.fproduct_img img').eq(jQuery('a.comas').index(this)).fadeIn();

            });
        } else {
            jQuery('.froduct_img_thumb img').click(function (index) {
                jQuery('.fproduct_img').find('img:visible').hide();
                jQuery('.fproduct_img img').eq(jQuery(this).index()).fadeIn();

            });
        }


    }

    jQuery('.required_fields span.red').addClass('red2');

    jQuery('.fproduct_img img').eq(0).show(); //показываю 1ю

    function sinh() {
        var a = jQuery('.flexslider li.flex-active-slide').index();

        jQuery('.fproduct_img img').css('display','none');
        jQuery('.fproduct_img img').eq(a).css('display','block');
    };
    
    function invert_sinh() {
        var b = jQuery('a.comas').index(jQuery('a.active_coma'));

        jQuery('.flexslider li.flex-active-slide').css('display','none');
        jQuery('ul.slides li').eq(b).css('display','block');
    };


    // обратно к товару
    jQuery('.backto').click(function () {


        sinh();
        jQuery.scrollTo('#wrapper', 500);
        return false;
    });

    // вопросы и ответы
    jQuery('.page h3').click(function () {

        var w = jQuery('body').width();
        jQuery(this).next().slideToggle();
        if (w < 958) {

            jQuery('.page h3').removeClass('last');
            jQuery('h1.title').hide();
//            var b = jQuery(this).find('a').html();
//            jQuery('h1.title span').html(b);

//            console.log(b);


            jQuery('.page_7 .sub-content h3').addClass('h3notactive');
            jQuery(this).addClass('h3active');
//             jQuery(this).next().show();

            jQuery('.page_7 .sub-content .sub-content').append('<div class="backlist">К СПИСКУ ВОПРОСОВ</div>');

            jQuery('.backlist').click(function () {

                jQuery('h1.title').show();
                jQuery('.page h3.h3active').next().slideToggle();
                jQuery('.page h3.h3active').addClass('last');
                jQuery('.page h3').removeClass('h3active');
                jQuery('.page h3').removeClass('h3notactive');
                jQuery('.backlist').remove();


                return false;
            });

        } else {


        }

        return false;

    });


    function myvalid(hu) {

//        console.log(hu);

//        if (hu == 'email') jQuery('.form_buy input[name="email"]').removeClass('active_f');
//        if (hu == 'phone') jQuery('.form_buy input[name="phone"]').removeClass('active_f');


        function validateEmail(email) //проверка что ящик
        {
            var re = /\S+@\S+\.\S+/;
            return re.test(email);
        }

        function validatePhone(phone) //проверка что телефон
        {
            if (phone === '') {
                return false;
            }
            else {

//            console.log(phone);
                var re = /^[0-9\-_\(\)\s]*$/;
                return re.test(phone);
            }
        }

        var error1 = "<span class='error'>Неправильно заполненное поле!</span>"
        var error2 = "<span class='error'>Некорректный адрес email</span>"
        var error3 = "<span class='error'>Используйте только цифры</span>"

//        jQuery('.error').remove(); //удаляю ошибки

        var mail = jQuery('.form_buy input[name="email"]');

        if (validateEmail(mail.val())) {
            mail.removeClass('false');
            mail.next().remove();
            mail.addClass('true');
            var checked1 = true;
            jQuery(this).parent().find('span.error').remove();
        } else {
            mail.addClass('false');
            var checked1 = false;
            mail.after(error2).fadeIn(500);

        }

        var phone = jQuery('.form_buy input[name="phone"]');
        var aaa = validatePhone(phone.val());

//        console.log(aaa);
//        if (parseInt(phone.val()) > 0) {
        if (aaa) {
            phone.next().remove();
            phone.removeClass('false');
            phone.addClass('true');
            var checked2 = true;
            jQuery(this).parent().find('span.error').remove();
        } else {
            phone.addClass('false');
            var checked2 = false;
            if (phone.val() == '') {

                phone.after(error1).fadeIn(500);
            } else {

                phone.after(error3).fadeIn(500);
            }

        }


        var chedked = checked1 * checked2; // общая проверка перед отправкой что ок

        return chedked;

    }

//	jQuery('.form_buy input,.form_buy textarea').is(":focus").focusout(function () {

    // ажимаю на кнопки 123

    jQuery(document).keypress(function (e) {
        var a = e.charCode - 49;
        coutnImages = jQuery(".fproduct_img img").length;
       // console.log(a);
        if ((a < coutnImages) 
            && (a>=0)
            ) {

            if (!jQuery('.form_buy input,.form_buy textarea').is(":focus")) {

                jQuery('.fproduct_img img').hide();
                jQuery('.fproduct_img img').eq(a).fadeIn();


            }

        }

    });



    function myfocus() {

        jQuery('.form_buy input[name="email"]').focusout(function () {
            myvalid(jQuery(this).attr('name'));
        });

        jQuery('.form_buy input[name="phone"]').focusout(function () {
            myvalid(jQuery(this).attr('name'));
        });

        jQuery('.form_buy input[name="phone"]').focus(function () {
            jQuery(this).parent().find('span.error').remove();
//            jQuery(this).addClass('active_f');
        });

        jQuery('.form_buy input[name="email"]').focus(function () {
            jQuery(this).parent().find('span.error').remove();
//            jQuery(this).addClass('active_f');
        });
    }

    myfocus();

    jQuery('.form_buy input').focusout(function () {
        jQuery('.form_buy input,.form_buy textarea').removeClass('active_f');
    });

    jQuery('.form_buy input').focus(function () {

        jQuery(this).addClass('active_f');
    });


    jQuery('.form_buy textarea').focusout(function () {
        jQuery('.form_buy .textarea_div').removeClass('active_f');
    });

    jQuery('.form_buy textarea').focus(function () {

        jQuery('.form_buy .textarea_div').addClass('active_f');
    });


  jQuery('.form_buy form').submit(function (e){

            e.preventDefault();

            return false;


    });

//    валидация формы отправки
    jQuery('.link-buy a,.dopsubmit').click(function (event) {

        // alert('123');

        var chedked = myvalid();

        if (chedked) {

            // данные
            var product = jQuery('.fproduct_title').html();
            var data = jQuery('.form_buy form').serialize();
            // Отсылаем паметры
            jQuery.ajax({
                type: "POST",
                url: "/blocks/ajax.php",
                data: data + '&product=' + product,
                // Выводим то что вернул PHP
                success: function (html) {
                    jQuery('.form_buy').append('<div class="decor_load"></div>');
                    var i = 0;
                    (function() {
                        if (i < 7) {
                            jQuery('.decor_load').html(i);
                            i++;
                            setTimeout(arguments.callee, 100);
                        } else {
                            // alert('Закончили');
                        } 
                    })();
//                        jQuery('.form_buy').delay(1000).empty();

                    setTimeout(function () {
                        jQuery('.form_buy').empty();
                        jQuery(".form_buy").append(html);


                    }, 700);

                },
                beforeSend: function () {
                    jQuery(".form_buy,.required_fields").empty();
                    $(window).unbind('scroll');

                    // jQuery('.form_buy').append('<div class="decor_load">1</div>');
                    // console.log('123');
                    // for (var i=7; i >= 1; i--) {
                    //     console.log(i);
                    //     setTimeout(function () {
                    //        jQuery('.decor_load').html(i);
                    //        console.log(i);

                    //     }, 500);
                     

                    // };
                }
            });

        jQuery(window).scroll(function () {
            scrollMenu();
        });

        }

        event.preventDefault();
        return false;
    });

    jQuery('.flex-direction-nav a').click(function () {
        sinh()
    });

    jQuery('.form_item_title label').click(function () {
        jQuery(this).parent().next().children().children().focus();
        jQuery(this).parent().next().children().focus();
    });


    jQuery('.mobile_menu a').click(function (event) {
        var hei = jQuery('.block-menu ul.menu').height();
        event.preventDefault();
        var scrolvalclick = jQuery(window).scrollTop();
        jQuery('.block-menu ul.menu').slideToggle(function (event) {
            if (scrolvalclick < 100) {
                if (hei == 270) {
                    jQuery('#content').css('margin-top', hei + 'px');
                } else {
                    jQuery('#content').css('margin-top', 0 + 'px');
                }
            }

        });

    });


    var scrollMenu = function () {

        var scrolval = jQuery(window).scrollTop();

        var w = jQuery('body').height();

//        if (scrolval < 100)  jQuery('.mainmobile_form').removeClass('mainmobile_form2');

        //alert(w);
        if (w < 300) {
//            var t = jQuery('.mainmobile_form2').position().top;

//            jQuery('.mainmobile_form2').css('position', 'static')
        } else {
//            jQuery('.mainmobile_form2').css('position', 'fixed')
        }


        var b = jQuery('.mainmobile_form2 .form_buy .mess_happy').hasClass('mess_happy');

        if (b) jQuery('.mainmobile_form2 .form_buy').empty().css('height','auto').hide();
        //при скролле
        //jQuery('.mainmobile_form').removeClass('mainmobile_form2');

        //if (jQuery('.mobile_menu').is(':visible')){

//                        console.log(scrolval);
        // console.log(scrolval);

        if (jQuery('.body-slider').offset() != null) {

            if (scrolval >= jQuery('.body-slider').offset().top) {
                jQuery('.backto').addClass('backto2');
            } else {
                jQuery('.backto').removeClass('backto2');

            }
        } 

      if (scrolval >= 100) {

            jQuery('#wrapper').addClass('sub-content11');

        }
        if (scrolval < 100) {

            jQuery('#wrapper').removeClass('sub-content11');
 
        }

    };

    jQuery(window).scroll(function () {
        scrollMenu();
    });

    jQuery(window).resize(function () {
        scrollMenu();
    })

    jQuery('.page_7 .sub-content > h3').
        wrapInner('<a href="#"></a>').
        append('<span class="contr" data-icon="/" aria-hidden="true"></span><span class="contrm" data-icon="+" aria-hidden="true"></span>').
        wrapInner('<span class="main-url"></span>');

    jQuery('.mobile_menu a,.mob li a').append('<span class="point" data-icon="(" aria-hidden="true"></span>').prepend('<span class="point" data-icon="(" aria-hidden="true"></span>');

//    кнопка назад в мобильной версии при скролле
    jQuery('.return a.return').click(function () {
        //$(window).unbind('scroll');
      
        jQuery('.mainmobile_form').removeClass('mainmobile_form2');
//        jQuery('html,body').css("overflow-y","auto");
//        jQuery.scrollTo('#wrapper', 500);
    jQuery('#wrapper').removeClass('wrap');
        return false;
    });


    //кнопка меню при скролле в мобильной версии
    jQuery('.link_to_menu').click(function () {
        jQuery('ul.menu').slideToggle();
        return false;
    });

    jQuery('.mob-cat').change(function () {

        var filter = jQuery(this).val();
//        console.log(filter);
        if (filter == 'М') jQuery('.cat_w').trigger('click');
        if (filter == 'Ж') jQuery('.cat_m').trigger('click');
        if (filter == '0') {
            jQuery('.prod_w,.prod_m').removeClass('hide');
            jQuery('.cat_w,.cat_m').removeClass('active');
        }
        ;


    });

//    jQuery('select').selectbox();
//    jQuery('div.select').append('<span data-icon="/" aria-hidden="true"></span>');

    jQuery('span.ui-btn-inner').append('<span data-icon="/" aria-hidden="true"></span>');

    jQuery('.froduct_img_thumb img:last').addClass('img-last');

//    jQuery('.mainmobile_form').append('');

    function OffScroll() {
        var winScrollTop = $(window).scrollTop();
        $(window).bind('scroll', function () {
            $(window).scrollTop(winScrollTop);
        });
    }

        // alert('1');
    jQuery('.buy_div a,.prod_link a,.prod_img > a').click(function () {

        // alert('1');

        var w = jQuery('body').width();
        if (w < 739) {


      

        jQuery.scrollTo('#wrapper', 500);

            jQuery('#wrapper').addClass('wrap');
            
            // if (jQuery('.mainmobile_form div div').hasClass('mess_happy')) {

                jQuery(".mainmobile_form .form_buy").empty().show();

                jQuery('.mainmobile_form .form_buy').html((jQuery('#footer .form_buy_mobile form').html()));

                jQuery('.link-buy a').click(function (event) {



                    // alert('1');
                    jQuery.scrollTo('#wrapper', 0);

                    var chedked = myvalid();

                    if (chedked) {
                    
                        jQuery.scrollTo('#wrapper', 0);
                        // данные
                        var product = jQuery('.fproduct_title').html();
                        var data = jQuery('.form_buy form').serialize();
                        // Отсылаем паметры
                        jQuery.ajax({
                            type: "POST",
                            url: "/blocks/ajax.php",
                            data: data + '&product=' + product,

                            success: function (html) {              
                                jQuery('.form_buy').empty().css('height','248px');
                                jQuery('.form_buy').append('<div class="decor_load"></div>');
                                var i = 0;
                                (function() {
                                    if (i < 7) {
                                        jQuery('.decor_load').html(i);
                                        i++;
                                        setTimeout(arguments.callee, 100);
                                    } else {
                                        // alert('Закончили');
                                    } 
                                })();
//                        jQuery('.form_buy').delay(1000).empty();

                    setTimeout(function () {
                        jQuery('.form_buy').empty();
                        jQuery(".form_buy").append(html);


                    }, 700);

                            }
                        });

                    }

                    return false;
                });

                myfocus();

            // }

           jQuery('.mainmobile_form').addClass('mainmobile_form2');
            var a = jQuery(this).attr("href");
            var scu = (a.substr(9));
            jQuery('.mainmobile_form form [name="scu"]').val(scu);
            jQuery.scrollTo('#wrapper', 0);
             return false;


        }

    });

    jQuery(".mainmobile_form").clone()             
        .addClass("mainmobile_form3")
        .removeClass("mainmobile_form").
        children().removeClass('form_buy')
        .appendTo("#footer");



//    jQuery('.mainmobile_form2').on('focus','input',(function () {
////        jQuery(this).parent().find('span.error').remove();
////            jQuery(this).addClass('active_f');
//        alert('1');
//    }));

  jQuery('.form_buy_mobile .link-buy a').click(function(){
    jQuery.scrollTo('#wrapper', 0);
  });

    last_item();

    

       
        imgs = $("body");

        //Init touch swipe
        imgs.swipe( {
            triggerOnTouchEnd : true,
            swipeStatus : swipeStatus,
            allowPageScroll:"vertical"
        });


});
  function last_item(){

        jQuery('.products .prod').removeClass('last_in_cat');

        var wth = jQuery('body').width();

        if (wth < 958) {

        // jQuery('.products .prod:visible').each(function(i){ 
        //     if (((i+1) % 2)==0) jQuery(this).addClass('last_in_cat');
        // });

        } else {
            jQuery('.products .prod:visible').each(function(i){ 
            if (((i+1) % 4)==0) jQuery(this).addClass('last_in_cat');
        });
        }

        


    }


     