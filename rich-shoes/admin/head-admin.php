<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Bootstrap -->
<?php
$url = "http://".$_SERVER['HTTP_HOST'].'/admin';
$url2 = "http://".$_SERVER['HTTP_HOST'];
?>
<link rel="stylesheet" href="<?php print $url2 ?>/css/reset.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="<?php print $url2 ?>/css/main.css" type="text/css" media="screen, projection" />
<link href="<?php print $url ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">

<script src="http://code.jquery.com/jquery.js"></script>
<script src="<?php print $url ?>/bootstrap/js/bootstrap.min.js"></script>

<script src="<?php print $url ?>/ckeditor/ckeditor.js"></script>

<script src="<?php print $url ?>/js/template.js"></script>

<?php /*
<script src="<?php print $url ?>/editor/jquery.wysiwyg.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php print $url ?>/editor/src/dialogs/default.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="<?php print $url ?>/editor/controls/wysiwyg.image.js"></script>

<link rel="stylesheet" href="<?php print $url ?>/editor/jquery.wysiwyg.css" type="text/css" media="screen" charset="utf-8" />
<link rel="stylesheet" href="<?php print $url ?>/editor/src/dialogs/default.css" type="text/css" media="screen" charset="utf-8" />
*/?>

<link rel="stylesheet" href="<?php print $url ?>/css/admin.css" type="text/css" media="screen" charset="utf-8" />
