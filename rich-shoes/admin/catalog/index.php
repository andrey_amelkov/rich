<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title></title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <?php include_once '../../admin/head-admin.php' ?>
</head>
<?php 
session_start();
include_once '../../admin/config.php';
check_login();

?>
<body>

<?php include_once '../../admin/config.php' ?>

<div id="wrapper">
<div class="admin-head">
    <?php include_once '../../admin/blocks/head.php'; ?>
</div>
<!-- #header-->

<div id="content">
<div class="hero-unit">
    <form enctype="multipart/form-data" action="/admin/catalog/" method="post">
        <div>
            Импорт
        </div>
        <div>
            <input type="file" name="file" id=""
                   accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>
        </div>
        <div>
            <input class="btn" type="submit" value="Загрузить"/>
        </div>
    </form>


    <div>
        csv Файлы залитые на сервер
    </div>

    <?php

    $csv = scan_file($_SERVER['DOCUMENT_ROOT'] . '/csv/', 'csv');
    //            dpm($csv);
    ?>

    <?php foreach ($csv as $val) : ?>

        <div>
            <a target="_blank" href="<?php print $url2 . '/csv/' . $val ?>"><?php print $val ?></a>
        </div>

    <?php endforeach;?>

    <?php //include_once '../../admin/blocks/content.php'; ?>
</div>

<?php

$products_for_template = array(); //массив для шаблона

//доп валидация cvs

$cvs_error = cvs_valid();
$valid = (count($cvs_error) > 0) ? false : true;

// print_r($valid);
//dpm($_FILES);
?>

<?php if (!$valid) : ?>

    <div class="csv_error">
        В процессе обновления каталога были ошибки, которые не позволили его обновить <br>
        Ошибки в строках
        <?php

        foreach ($cvs_error as $val) {
            print $val . ',';
        }
        ?>
    </div>

<?php else: ?>
<?php

    $a = explode('.',$_FILES['file']['name']);
    if ($a[1] == 'csv') :?>
    <div class="csv_ok">
        Каталог успешно обновлен
    </div>
<?php endif;?>
<?php endif;?>


<?php

 // print_r ($_FILES['file']['type']);
 // print_r($valid);

if ((($_FILES['file']['type']) == 'text/csv') && ($valid)) {

    foreach ($img_size as $key => $value) {

        $path = $_SERVER['DOCUMENT_ROOT']."/products/preset_".$value;

        if ($handle = opendir($path)) {
        while (false !== ($file = readdir($handle))) { 
            if ($file != "." && $file != "..") { 
                 unlink ($path.'/'.$file); 


            }

            // print_r($path.'/'.$file); echo '</br>';
        }
        closedir($handle); 
        }
    }



    $furl = $_FILES['file']['tmp_name'];

    copy($furl, $_SERVER['DOCUMENT_ROOT'] . '/csv/' . $_FILES['file']['name']);

    $file_array = file($furl);

    $a = array();

    //еперекодировка
    foreach ($file_array as $val) {
        $a[] = iconv('cp1251', 'utf-8', $val);
    }

    $file_array = $a;


    $data = array(); //Все
    $art = array(); // АРТИКУЛЫ
    foreach ($file_array as $row) {
        $tmp_arr = explode(';', $row);
        $data[] = $tmp_arr;
        $art[] = $tmp_arr[0];
    }


    unset($data[0]);

    $files_i = array();

    $up_dir = $_SERVER['DOCUMENT_ROOT'] . '/upload/';

    $files = scan_file($up_dir, 'jpg|png|gif'); // на выходе файлы по маске

    // dpm($files);

    ?>

    <?php

    $up_dir2 = $_SERVER['DOCUMENT_ROOT'] . '/products';
    foreach ($img_size as $val) {
        $dir = $up_dir2 . '/preset_' . $val;
        if (!file_exists($dir)) {
            mkdir($dir, 0755); // создаем папки для ресайза
        }
    }

    // include_once $_SERVER['DOCUMENT_ROOT'] . '/admin/classSimpleImage.php';

    unset($art[0]);

    $prod = array();
    $prod2 = array();
    $prod4 = array();

    $files_no_import = array();

    // dpm($files);

    foreach ($files as $key => $val) {

        $name = explode(".", $val);

        // дополнительная проверка на  -1..-2.. и просто по имени
        $check_name = check_name($name[0], $art);

        // if (!$check_name) {
        //     $files_no_import[] = $name[0];
        // }

        // dpm($name[0]);

        // dpm($check_name);

        if ($check_name) {

            //ресайжу

            $prod[] = $name[0] . '.' . $name[1];
            $prod2[] = $name[0];
            $prod4[] = $name;

            ?>

<!--            <input class="size" type="text" name="size" value="--><?php //print $val ?><!--"/>-->
            <?php //foreach ($img_size as $size) : ?>



<?php
                //  $size = "200x200";
                // $mas_size = explode("x", $size);

                // $image = new SimpleImage();
                // $image->load($_SERVER['DOCUMENT_ROOT'] . '/upload/' . $val);
                // $image->resize($mas_size[0], $mas_size[1]);
                // $image->save($_SERVER['DOCUMENT_ROOT'] . '/products/preset_' . $size . '/' . $name[0] . '-' . $size . '.' . $name[1]);
                ?>
            <?php //endforeach; ?>


        <?php
        }

    }

    // dpm($files_no_import);

    $out_prod = array();
    $prod3 = array();


    foreach ($prod4 as $val) {

        $name = explode("-", $val[0]);
        $prod3[$name[0]][$name[1]] = $val[0] . '.' . $val[1];


    }

    $prod5 = array();


    foreach ($prod3 as $key => $val) {
        $prod5[] = $key;
    }

    $prod7 = array();
    $i = 0;
    foreach ($prod3 as $val) {
        $prod7[$i] = $val;
        $i++;
    }

    $prod6 = array();

    foreach ($data as $val) {

        $key2 = array_search($val[0], $prod5);

        if ($key2 > (-1)) {

            $temp = $val;
            array_push($temp, $prod7[$key2]);
            $prod6[] = $temp;

        } else{
            $files_no_import[] = $val;
        }

    }

    $out_prod = $prod6;

    // dpm($files_no_import);

    generate_for_cat(); // генерация товаров для каталога
    generate_item_prod($out_prod); //генерация страниц товаров

    // generate_small($out_prod);

//
}
?>

<?php

$worning_f = "";


if (count($files_no_import)>0) {
    
foreach ($files_no_import as $key => $value) {
    $worning_f .= $value[0].', ';

}
}

$worning_f =  substr($worning_f, 0, strlen($worning_f)-2);

?>


<?php if (count($files_no_import)>0) :?>
<div class="worning">

    Для файлов <?php print $worning_f;?> не загружен соответствующий файл в папку upload

</div>

<?php endif;?>

</div>
<!-- #content-->

<?php if ($valid) : ?>

<!--    <script type="text/javascript">-->
<!--        jQuery(document).ready(function () {-->
<!--            jQuery('.size').each(function (index) {-->
<!--                var data = jQuery(this).val();-->
<!---->
<!--                jQuery.ajax({-->
<!--                    type: "POST",-->
<!--                    url: "resize.php",-->
<!--                    data: "name=" + data-->
<!---->
<!--                });-->
<!---->
<!--            });-->
<!---->
<!--        });-->
<!--    </script>-->
<?php endif; ?>

</div>
<!-- #wrapper -->


<div id="footer">
    <?php include_once '../../admin/blocks/footer.php'; ?>
</div>
<!-- #footer -->


</body>
</html>